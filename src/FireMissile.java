import org.newdawn.slick.SlickException;

/**
 * Interface that needs to be in a class that can fire missiles.
 * @author sandeharsa
 *
 */
public interface FireMissile {
    /** Fire a missile 
     * @throws SlickException */
    public Missile fire() throws SlickException;

    /** Get cooldown of firing missile */
	public double getcooldown();
	
    /** Update cooldown 
     * @param delta Time passed in the game world.
     * */
	public void update_cooldown(int delta);
}
