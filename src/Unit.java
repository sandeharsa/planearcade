/**
 * Basic class for units in the game.
 * @author sandeharsa
 *
 */
public class Unit extends GameObject {
    /** Unit's stats */
    protected int fullShield;
    protected int shield;
    protected int firepower;
    protected int damage;
    protected double speed;
    
    /** The fully-repaired value of the unit's shield */
    public int getFullShield()
    {
    	return fullShield;
    }
   
    /** The current value of the unit's shield */
    public int getShield()
    {
    	return shield;
    }
    
    /** The firepower determines how rapidly the unit can fire missiles (set to -1 if the unit cannot fire) */
    public int getFirePower()
    {
    	return firepower;
    }
    
    /** The amount of damage the unit causes when it collides with other units */
    public int getDamage()
    {
    	return damage;
    }
    
    /** The number of pixels the unit may move per millisecond. */
    public double getSpeed()
    {
    	return speed;
    }
    
    /** Apply damage to shield 
     * @param damage The damage dealt
     * */
    public void damageShield(double damage)
    {
    	this.shield -= damage;
    	if (this.shield < 0)
    		this.shield = 0;
    }
    
    @Override
    void incollision(GameObject obj)
    {
    	if (obj instanceof Unit) 
    	{
    		while(((Unit) obj).getShield() > 0 && this.getShield() > 0)
    		{
    			((Unit)obj).damageShield(this.getDamage());
    			this.damageShield(((Unit)obj).getDamage());
    		}
    	}
    }
    
    /** Indicates wheather the unit is dead or alive*/
	public boolean isdead() {
		return this.getShield() == 0;
	}
}

