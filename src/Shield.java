import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Repair: Restores your ship's Shield to Full-Shield, repairing any damage your ship has suffered.
 * @author sandeharsa
 *
 */
public class Shield extends Item {
	/** File path of the item image file. */
    private static final String image_file
        = Game.ASSETS_PATH + "/items/shield.png";
    
    /** Creates a new Repair.
     * @param x The item's start location (x, pixels).
     * @param y The item's start location (y, pixels).
     */
	public Shield(double x, double y) 
		throws SlickException 
	{
		super(x, y);
		this.img = new Image(image_file);
	}
	
	@Override
	public void effect(Player p)
	{
		p.shield += 40;
		p.fullShield += 40;
		
	}
}
