/**
 * An abstract class for game objects such as Unit and Item.
 * @author sandeharsa
 * 
 */

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

public abstract class GameObject
{
	/** x position of the object (in pixel) */
	protected double x;
	/** y position of t he object (in pixel) */
	protected double y;
	/** Image for the object */
	protected Image img;
	
	/**
	 * Action to do when the GameObject collides with another object
	 * @param obj The object
	 */
	abstract void incollision(GameObject obj);
	
	/** Render the object to the screen 
     * @param g The current Graphics context.
     * @param cam Camera used to render the current screen.
	 */
	public void render(Graphics g, Camera cam) {
        // Calculate the unit's on-screen location from the camera
        int screen_x, screen_y;
        screen_x = (int) (this.x - cam.getLeft());
        screen_y = (int) (this.y - cam.getTop());
        img.drawCentered(screen_x, screen_y);   
	}
	
	/**
	 * Check whether the object intersects (colliding) with another object
	 * @param obj
	 * @return boolean of whether the 2 objects is colliding or not
	 */
	public boolean intersects(GameObject obj)
	{
		// Create rectangle objects
		Rectangle r = new Rectangle((int)this.getX(), (int)this.getY(), this.img.getWidth(), this.img.getHeight());
		Rectangle p = new Rectangle((int)obj.getX(), (int)obj.getY(), obj.img.getWidth(), obj.img.getHeight());
		
		// Check whether the rectangles intersects
		return r.intersects(p);
	}
	
    /** The x coordinate of the GameObject, relative to map (pixels). */
    public double getX()
    {
        return x;
    }
    /** The y coordinate of the GameObject, relative to map (pixels). */
    public double getY()
    {
        return y;
    }
}
