import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Drone: Automated imperial seek-and-destroy robots.
 * They can't shoot, but they move directly on a collision course for the player. Their movement is calculated by an algorithm.
 * @author sandeharsa
 *
 */
public class Drone extends Enemy 
{
	/** File path of the Drone ship image file. */
    private static final String image_file
        = Game.ASSETS_PATH + "/units/drone.png";
    
    public Drone(double x, double y)
    	throws SlickException
    {
    	super(x, y);
    	this.img = new Image(image_file);
    	this.speed = 0.2;
    	// Set stats
    	this.damage = 8;
    	this.shield = 16;
    	this.firepower = -1;
    }
    
    /** Move the unit, according to its attack strategy.
     * Prevents the unit from moving onto blocking terrain,
     * or outside of the screen (camera) bounds.
     * @param world The world the unit is on (to check blocking).
     * @param cam The current camera (to check blocking).
     * @param dir_x The unit's movement in the x axis (-1, 0 or 1).
     * @param dir_y The unit's movement in the y axis (-1, 0 or 1).
     * @param delta Time passed since last frame (milliseconds).
     * @throws SlickException 
     */
    public void update(World world, Camera cam, int delta) throws SlickException
    {
    	super.update(world, cam, delta);
    	// Update only when on screen
    	if(world.OnScreen(this)) {
	    	// Distance between the drone and the player
	    	double distx = world.getPlayer().getX() - this.getX();
	    	double disty = world.getPlayer().getY() - this.getY();
	        // Calculate the amount the drone may move
	        double amount = delta * this.getSpeed();
	        // Total distance
	        double dist_total = Math.sqrt(Math.pow(distx, 2) + Math.pow(disty, 2));
	        // The new location 
	        double x = this.x + distx / dist_total * amount;
	        double y = this.y + disty / dist_total * amount;
	        moveto(world, x, y);
    	}
    }
}
