import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Missiles are projectiles that can be fired by some Units
 * @author sandeharsa
 *
 */

public class Missile extends GameObject {
	/** The damage that the missile inflicts */
	private int damage = 8;
	/** Flag to indicate whether the missile came from an enemy or a Player */
	private boolean enemy = false;
	/** Offset of the missile from the firing unit when first created */
	private double offset = 50;
	/** The speed of the missile */
	private double speed = 0.7;
    /** File path of the player's missile image file. */
    private static final String image_player
        = Game.ASSETS_PATH + "/units/missile-player.png";
    /** File path of the enemy's missile image file. */
    private static final String image_enemy
        = Game.ASSETS_PATH + "/units/missile-enemy.png";
	/**
	 * Create a missile in a position relative to a Unit
	 * @param u Unit that fires the missile
	 * @throws SlickException 
	 */
	public Missile(Unit u) 
		throws SlickException
	{
		this.x = u.getX();
		this.y = u.getY();
		if (u instanceof Player)
		{
			this.img = new Image(image_player);
			this.y -= offset;
		} 
		else 
		{
			this.img = new Image(image_enemy);
			enemy = true;
			this.y += offset;
		}
	}
	
	public int getDamage()
	{
		return damage;
	}
	
	/**
	 * Move the missiles upward/downward.
	 * Check collisions with the opposite type (player-enemy, enemy-player)
	 * Remove if no longer on screen.
	 * @param world The game world.
	 * @param delta Time passed in the game world.
	 */
	public void update(World world, int delta)
	{
    	if (world.OnScreen(this))
    	{
    		if(this.isEnemy() && world.getPlayer().intersects(this))
    		{
    			this.incollision(world.getPlayer());
    			world.getMissileList().remove(this);
    		} 
    	} 
    	else
    	{
    		world.getMissileList().remove(this);
    	}
		double x = this.x;
		double y = this.y;
		double amount = delta * speed;
		if(isEnemy())
			y += amount;
		else
			y -= amount;
		moveto(world, x, y);
	}
	
	public boolean isEnemy() 
	{
		return enemy;
	}
	
	@Override
	void incollision(GameObject obj)
	{
		// If the Player is getting hit and this is an enemy missile
		if (obj instanceof Player && this.isEnemy())
		{
			((Player)obj).damageShield(this.getDamage());
		}
		// Else if an enemy is getting hit and this is a player missile
		else if(obj instanceof Enemy && !this.isEnemy())
		{
			((Enemy)obj).damageShield(this.getDamage());
		}
	}
	
    /** Update the missile's x and y coordinates.
     * Destroys missiles that are moving onto blocking terrain.
     * @param world The world the player is on (to check blocking).
     * @param x New x coordinate.
     * @param y New y coordinate.
     */
    protected void moveto(World world, double x, double y)
    {
        // If the destination is not blocked by terrain, move there
        if (!world.terrainBlocks(x, y))
        {
            this.x = x;
            this.y = y;
        }
        // Else (it cannot move anywhere) delete.
        else
        {
        	world.getMissileList().remove(this);
        }
    }
}
