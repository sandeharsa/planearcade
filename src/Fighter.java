/**
 * Fighters: These Vaaj strike craft have come to shoot you down. 
 * They always move downwards, and fire their lasers continuously.
 * @author sandeharsa
 *
 */

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Fighter extends Enemy implements FireMissile
{
    /** File path of the fighter ship image file. */
    private static final String image_file
        = Game.ASSETS_PATH + "/units/fighter.png";
    /** Fire missile cooldown */
    double missile_cd;
    
    public Fighter(double x, double y)
    	throws SlickException
    {
    	super(x, y);
    	this.img = new Image(image_file);
    	this.speed = 0.2;
    	// Set stats
    	this.damage = 9;
    	this.shield = 24;
    	this.firepower = 0;
    }
    
    /** Move the unit, according to its attack strategy.
     * Prevents the unit from moving onto blocking terrain,
     * or outside of the screen (camera) bounds.
     * @param world The world the unit is on (to check blocking).
     * @param cam The current camera (to check blocking).
     * @param dir_x The unit's movement in the x axis (-1, 0 or 1).
     * @param dir_y The unit's movement in the y axis (-1, 0 or 1).
     * @param delta Time passed since last frame (milliseconds).
     * @throws SlickException 
     */
    public void update(World world, Camera cam, int delta) throws SlickException
    {
    	super.update(world, cam, delta);
    	// Update only when on screen
    	if(world.OnScreen(this)) {
	        /* Calculate the amount to move in each direction, based on speed */
	        double amount = delta * this.getSpeed();
	        /* The new location */
	        double x = this.x;
	        double y = this.y + amount;
	        moveto(world, x, y);
	        /* Fire missiles */
			if (this.getcooldown() == 0)
				world.missileList.add(this.fire());
	        /** Update missile cooldown */
	        update_cooldown(delta);
    	}
    }
    
    @Override
    public Missile fire()
    	throws SlickException
    {
    	missile_cd = (300 - 80 * this.getFirePower());
    	return new Missile(this);
    }

    @Override
	public double getcooldown() {
    	return missile_cd;
	}
	
    @Override
	public void update_cooldown(int delta) {
        if (missile_cd > 0)
        	missile_cd -= delta;
        if (missile_cd < 0)
        	missile_cd = 0;
	}
}
