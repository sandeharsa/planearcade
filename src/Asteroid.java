import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Asteroid: Big chunks of rock that can punch a hole right through your ship.
 * They simply move downwards, and can't fire
 * @author sandeharsa
 *
 */

public class Asteroid extends Enemy
{
	/** File path of the asteroid ship image file. */
    private static final String image_file
        = Game.ASSETS_PATH + "/units/asteroid.png";
    
    public Asteroid(double x, double y)
    	throws SlickException
    {
    	super(x, y);
    	this.img = new Image(image_file);
    	this.speed = 0.2;
    	// Set stats
    	this.damage = 12;
    	this.shield = 24;
    	this.firepower = -1;
    }
    
    /** Move the unit, according to its attack strategy.
     * Prevents the unit from moving onto blocking terrain,
     * or outside of the screen (camera) bounds.
     * @param world The world the unit is on (to check blocking).
     * @param cam The current camera (to check blocking).
     * @param dir_x The unit's movement in the x axis (-1, 0 or 1).
     * @param dir_y The unit's movement in the y axis (-1, 0 or 1).
     * @param delta Time passed since last frame (milliseconds).
     * @throws SlickException 
     */
    public void update(World world, Camera cam, int delta) throws SlickException
    {
    	super.update(world, cam, delta);
    	// Update only when on screen
    	if(world.OnScreen(this)) {
	        /* Calculate the amount to move in each direction, based on speed */
	        double amount = delta * this.getSpeed();
	        /* The new location */
	        double x = this.x;
	        double y = this.y + amount;
	        moveto(world, x, y);
    	}
    }
}
