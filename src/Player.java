/* SWEN20003 Object Oriented Software Development
 * Space Game Engine (Sample Project)
 * Author: Matt Giuca <mgiuca>
 */

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/** The ship which the player controls.
 */
public class Player extends Unit implements FireMissile
{
    /** File path of the player's ship image file. */
    private static final String image_file
        = Game.ASSETS_PATH + "/units/player.png";
    /** Initial full-shield and shield */
    public static final int INIT_SHIELD = 100;
    /** Initial firepower */
    public static final int INIT_FIREPOWER = 0;
    /** Maximum firepower (after upgrade) */
    public static final int MAX_FIREPOWER = 3;
    /** Missile cooldown */
    double missile_cd;
    /** Creates a new Player.
     * @param x The player's start location (x, pixels).
     * @param y The player's start location (y, pixels).
     */
    public Player(double x, double y)
        throws SlickException
    {
        this.img = new Image(image_file);
        this.speed = 0.4;
        this.x = x;
        this.y = y;
        // Set stats
        this.fullShield = INIT_SHIELD;
        this.damage = 10;
        this.shield = INIT_SHIELD;
        this.firepower = INIT_FIREPOWER;
    }

    /** Move the player automatically forwards, as well as (optionally) in a
     * given direction. Prevents the player from moving onto blocking terrain,
     * or outside of the screen (camera) bounds.
     * @param world The world the player is on (to check blocking).
     * @param cam The current camera (to check blocking).
     * @param dir_x The player's movement in the x axis (-1, 0 or 1).
     * @param dir_y The player's movement in the y axis (-1, 0 or 1).
     * @param delta Time passed since last frame (milliseconds).
     * @throws SlickException 
     */
    public void update(World world, Camera cam, double dir_x, double dir_y,
    	boolean fire, int delta) throws SlickException
    {
        /* Calculate the amount to move in each direction, based on speed */
        double amount = delta * getSpeed();
        /* The new location */
        double x = this.x + dir_x * amount;
        double y = this.y + dir_y * amount;
        if (!world.reachedTop())
            y -= delta * getAutoSpeed();
        // Check if the player is off the screen, and push back in
        if (x < cam.getLeft())
            x = cam.getLeft();
        if (x > cam.getRight() - 1)
            x = cam.getRight() - 1;
        if (y < cam.getTop() + (img.getHeight() /2.0)) // check the top edge
            y = cam.getTop() + (img.getHeight() / 2.0);
        if (y > cam.getBottom() - 1 - (img.getHeight() / 2.0)) // check the bottom edge
            y = cam.getBottom() - 1 - (img.getHeight() / 2.0);
        // if fire is true, fire missiles from the player
        if(fire && this.getcooldown() == 0)
        	world.getMissileList().add(this.fire());
        update_cooldown(delta);
        
        moveto(world, x, y);
    }
    
    /** Update the unit's x and y coordinates.
     * Prevents the unit from moving onto blocking terrain.
     * @param world The world the player is on (to check blocking).
     * @param x New x coordinate.
     * @param y New y coordinate.
     */
    protected void moveto(World world, double x, double y)
    {
        // Set offsets
        double offsetX = img.getWidth() / 2.0 - 1; // -1 to let the blocking be less strict
    	double offsetY = img.getHeight() / 2.0 - 1; // -1 to let the blocking be less strict
        // If the destination is not blocked by terrain, move there
        if (!world.terrainBlocks(x + offsetX, y) && !world.terrainBlocks(x - offsetX, y) 
        		&& !world.terrainBlocks(x, y + offsetY) && !world.terrainBlocks(x, y - offsetY))
        {
            this.x = x;
            this.y = y;
        }
        // Else: Try moving horizontally only
        else if (!world.terrainBlocks(x + offsetX, this.y) && !world.terrainBlocks(x - offsetX, this.y) 
        			&& !world.terrainBlocks(x, this.y + offsetY) && !world.terrainBlocks(x, this.y - offsetY))
        {
            this.x = x;
        }
        // Else: Try moving vertically only
        else if (!world.terrainBlocks(this.x, y + offsetY) && !world.terrainBlocks(this.x, y - offsetY)
        			&& !world.terrainBlocks(this.x + offsetX, y) && !world.terrainBlocks(this.x - offsetX, y))
        {
            this.y = y;
        }
    }

    /** The number of pixels the player automatically moves per millisecond.
     */
    private double getAutoSpeed()
    {
        return 0.25;
    }
    
    @Override
    public Missile fire()
    	throws SlickException
    {
    	missile_cd = (300 - 80 * this.getFirePower());
    	return new Missile(this);
    }

    @Override
	public double getcooldown() {
    	return missile_cd;
	}
	
    @Override
	public void update_cooldown(int delta) {
        if (missile_cd > 0)
        	missile_cd -= delta;
        if (missile_cd < 0)
        	missile_cd = 0;
	}
	
	/**
	 * Reset the Player's shield to its full-shield and teleports the Player
	 * @param x Coordinate to teleport.
	 * @param y Coordinate to teleport.
	 */
	public void reset(double x, double y)
	{
		this.shield = INIT_SHIELD;
		this.fullShield = INIT_SHIELD;
		this.firepower = INIT_FIREPOWER;
		this.x = x;
		this.y = y;
	}
}
