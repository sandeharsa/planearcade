import org.newdawn.slick.SlickException;

/**
 * Enemy: Unit that attacks and damage the Player.
 * @author sandeharsa
 *
 */
public abstract class Enemy extends Unit 
{
	public Enemy(double x, double y)
	{
    	this.x = x;
    	this.y = y;
	}

    /** Move the unit, according to its attack strategy.
     * Prevents the unit from moving onto blocking terrain,
     * or outside of the screen (camera) bounds.
     * @param world The world the unit is on (to check blocking).
     * @param cam The current camera (to check blocking).
     * @param delta Time passed since last frame (milliseconds).
     * @throws SlickException 
     */
    public void update(World world, Camera cam, int delta) 
    		throws SlickException
    {
    	if (world.OnScreen(this))
    	{
    		// Check if intersects with the player	
    		if(world.getPlayer().intersects(this))
    			world.getPlayer().incollision(this);
    		// Check if intersects with other enemies
    		for(int i = 0; i < world.getEnemyList().size(); i++)
    		{
    			// Don't check with itself
    			if(!this.equals(world.getEnemyList().get(i)) && this.intersects(world.getEnemyList().get(i)))
    				this.incollision(world.getEnemyList().get(i));
    		}
    		// Check if intersects with Player missiles
    		for(int j = 0; j < world.getMissileList().size(); j++)
            {
    			if(!world.getMissileList().get(j).isEnemy() && this.intersects(world.getMissileList().get(j)))
    			{
    				world.getMissileList().get(j).incollision(this);
    				world.getMissileList().remove(j);
    			}
            }
    	}
        // Remove dead enemies from EnemyList
    	if(this.isdead())
    		world.getEnemyList().remove(this);
    }
    
    /** Update the unit's x and y coordinates.
     * Prevents the unit from moving onto blocking terrain.
     * @param world The world the player is on (to check blocking).
     * @param x New x coordinate.
     * @param y New y coordinate.
     */
    protected void moveto(World world, double x, double y)
    {
        // Set offsets
        double offsetX = img.getWidth() / 2.0 - 1; // -1 to let the blocking be less strict
    	double offsetY = img.getHeight() / 2.0 - 1; // -1 to let the blocking be less strict
        // If the destination is not blocked by terrain, move there
        if (!world.terrainBlocks(x + offsetX, y) && !world.terrainBlocks(x - offsetX, y) 
        		&& !world.terrainBlocks(x, y + offsetY) && !world.terrainBlocks(x, y - offsetY))
        {
            this.x = x;
            this.y = y;
        }
        // Else: Apply damage to itself
        else 
        {
        	this.damageShield(this.getDamage());
        }
    }
}
