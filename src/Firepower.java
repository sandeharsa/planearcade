import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Firepower: Restores your ship's Shield to Full-Shield, Firepowering any damage your ship has suffered.
 * @author sandeharsa
 *
 */
public class Firepower extends Item {
	/** File path of the item image file. */
    private static final String image_file
        = Game.ASSETS_PATH + "/items/firepower.png";
    
    /** Creates a new Firepower.
     * @param x The item's start location (x, pixels).
     * @param y The item's start location (y, pixels).
     */
	public Firepower(double x, double y) 
		throws SlickException 
	{
		super(x, y);
		this.img = new Image(image_file);
	}

	@Override
	public void effect(Player p)
	{
		if (p.firepower < Player.MAX_FIREPOWER)
			p.firepower += 1;
	}
}
