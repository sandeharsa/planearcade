/* SWEN20003 Object Oriented Software Development
 * Space Game Engine (Sample Project)
 * Author: Matt Giuca <mgiuca>
 */

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

import java.util.ArrayList;

/** Represents the entire game world.
 * (Designed to be instantiated just once for the whole game).
 */
public class World
{
	/** Checkpoints (x-coordinate) */
	public static final double[] checkpoints = {13176, 9656, 7812, 5796, 2844};
	private int current_checkpoint = 0;
    /** Map, containing terrain tiles. */
    private TiledMap map;
    /** The player's ship. */
    private Player player;
    /** The camera. */
    private Camera camera;
    /** A list of enemies */
    public ArrayList<Enemy> enemyList;
    /** A list of items */
    private ArrayList<Item> itemList;
    /** A list of missiles */
    public ArrayList<Missile> missileList;
    
    /** Get the width of the game world in pixels. */
    public int getWidth()
    {
        return map.getWidth() * map.getTileWidth();
    }

    /** Get the height of the game world in pixels. */
    public int getHeight()
    {
        return map.getHeight() * map.getTileHeight();
    }
    /** Get the player that is in the game world */
    public Player getPlayer()
    {
    	return player;
    }
    /** Get the list of enemies that is in the game world. */
    public ArrayList<Enemy> getEnemyList()
    {
    	return enemyList;
    }
    /** Get the list of items that is in the game world. */
    public ArrayList<Item> getItemList()
    {
    	return itemList;
    }
    /** Get the list of missiles that is in the game world. */
    public ArrayList<Missile> getMissileList()
    {
    	return missileList;
    }
    

    /** Create a new World object. 
     * @throws SlickException */
    public World()
    throws SlickException
    {
        map = new TiledMap(Game.ASSETS_PATH + "/map.tmx", Game.ASSETS_PATH);
        player = new Player(1296, 13716);
        // Create a camera, centred and with the player at the bottom
        camera = new Camera(this, player);
        // Create list of enemies
        enemyList = new ArrayList<Enemy>();
        generateUnit.generateEnemy(enemyList);
        // Create list of items
        itemList = new ArrayList<Item>();
        generateUnit.generateItem(itemList);
        // Create list of missiles
        missileList = new ArrayList<Missile>();
    }

    /** True if the camera has reached the top of the map. */
    public boolean reachedTop()
    {
        return camera.getTop() <= 0;
    }

    /** Update the game state for a frame.
     * @param dir_x The player's movement in the x axis (-1, 0 or 1).
     * @param dir_y The player's movement in the y axis (-1, 0 or 1).
     * @param delta Time passed since last frame (milliseconds).
     */
    public void update(double dir_x, double dir_y, boolean fire, int delta)
    throws SlickException
    {
        // Move the camera automatically
        camera.update(delta);

        // Move the player automatically, and manually (by dir_x, dir_y)
        player.update(this, camera, dir_x, dir_y, fire, delta);
        
        // Centre the camera (in x-axis) over the player and bound to map
        camera.follow(player);
        
        // Update enemies
        for(int i = 0; i < enemyList.size(); i++)
       		enemyList.get(i).update(this, camera, delta);
        
        // Update items
        for(int i = 0; i < itemList.size(); i++)
        	itemList.get(i).update(this);
        
        // Update missiles
        for(int i = 0; i < missileList.size(); i++)
        	missileList.get(i).update(this, delta);
        
        // Update checkpoint
        if(current_checkpoint < checkpoints.length-1 && player.getY() <= checkpoints[current_checkpoint+1])
        	current_checkpoint += 1;
        
        // If the Player is dead or no longer on screen (stuck by block tiles), go to checkpoint
        if(player.isdead() || !OnScreen(player))
        	this.toCheckPoint();
    }

    /** Render the entire screen, so it reflects the current game state.
     * @param g The Slick graphics object, used for drawing.
     * @param textrenderer A TextRenderer object.
     */
    public void render(Graphics g, Panel p)
    throws SlickException
    {
        // Calculate the camera location (in tiles) and offset (in pixels)
        int cam_tile_x = (int) camera.getLeft() / map.getTileWidth();
        int cam_offset_x = (int) camera.getLeft() % map.getTileWidth();
        int cam_tile_y = (int) camera.getTop() / map.getTileHeight();
        int cam_offset_y = (int) camera.getTop() % map.getTileHeight();
        // Render w+1 x h+1 tiles (where w and h are 12x9; add one tile extra
        // to account for the negative offset).
        map.render(-cam_offset_x, -cam_offset_y, cam_tile_x, cam_tile_y,
            getScreenTileWidth()+1, getScreenTileHeight()+1);
        // Render the enemies
        for(int i = 0; i < enemyList.size(); i++)
        	enemyList.get(i).render(g, camera);
        // Render the items
        for(int i = 0; i < itemList.size(); i++)
        	itemList.get(i).render(g, camera);
        // Render the missiles
        for(int i = 0; i < missileList.size(); i++)
        	missileList.get(i).render(g, camera);
        // Render the player
        player.render(g, camera);
        // Render the Panel
        p.render(g, player.getShield(), player.getFullShield(), player.getFirePower());
    }

    /** Determines whether a particular map location blocks movement due to
     * terrain.
     * @param x Map x coordinate (in pixels).
     * @param y Map y coordinate (in pixels).
     * @return true if the location blocks movement due to terrain.
     */
    public boolean terrainBlocks(double x, double y)
    {
        int tile_x = (int) x / map.getTileWidth();
        int tile_y = (int) y / map.getTileHeight();
        // Check if the location is off the map. If so, assume it doesn't
        // block (potentially allowing ships to fly off the map).
        if (tile_x < 0 || tile_x >= map.getWidth()
            || tile_y < 0 || tile_y >= map.getHeight())
            return false;
        // Get the tile ID and check whether it blocks movement.
        int tileid = map.getTileId(tile_x, tile_y, 0);
        String block = map.getTileProperty(tileid, "block", "0");
        return Integer.parseInt(block) != 0;
    }

    /** Get the width of the screen in tiles, rounding up.
     * For a width of 800 pixels and a tilewidth of 72, this is 12.
     */
    private int getScreenTileWidth()
    {
        return (Game.screenwidth / map.getTileWidth()) + 1;
    }

    /** Get the height of the screen in tiles, rounding up.
     * For a height of 600 pixels and a tileheight of 72, this is 9.
     */
    private int getScreenTileHeight()
    {
        return (Game.screenheight / map.getTileHeight()) + 1;
    }
    
    /**
     * Check whether an object is on screen or not.
     * @param obj The object.
     */
    public boolean OnScreen(GameObject obj)
    {
    	double yMin = camera.getTop();
    	double yMax = camera.getTop() + Game.screenheight;
    	double xMin = camera.getLeft();
    	double xMax = camera.getLeft() + Game.screenwidth;
        return (xMin < obj.getX() && obj.getX() < xMax && yMin < obj.getY() && obj.getY() < yMax);
    }
    
    /**
     * Teleports the Player to the nearest checkpoint.
     * Resets the status of all enemies and items.
     * Destroy all missiles.
     * @throws SlickException
     */
    private void toCheckPoint() 
    	throws SlickException
    {
    	// Reset the player to a new location
    	player.reset(1296, checkpoints[current_checkpoint]);
    	// Set a new camera
        camera = new Camera(this, player);
        // Reset list of items
        enemyList = new ArrayList<Enemy>();
        generateUnit.generateEnemy(enemyList);
        // Reset list of items
        itemList = new ArrayList<Item>();
        generateUnit.generateItem(itemList);
        // Create a new list of missiles
        missileList.removeAll(missileList);
    }
}