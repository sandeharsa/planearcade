import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * The Boss: The commander of Vaaj fleet won't let you escape easily.
 * He moves from side-to-side, firing rapidly and continuously.
 * He moves to the left until his x position is 1013 then moves to the right until his x position is 1589, and continuously repeats this process
 * @author sandeharsa
 *
 */

public class Boss extends Enemy implements FireMissile 
{
	/** File path of the Boss ship image file. */
    private static final String image_file
        = Game.ASSETS_PATH + "/units/boss.png";
    /** Boundary to move left and right */
    private double xMax = 1512;
    private double xMin = 1080;
    private int direction = -1; // -1 = left
    /** Missile cooldown */
    double missile_cd;
    
    public Boss(double x, double y)
    	throws SlickException
    {
    	super(x, y);
    	this.img = new Image(image_file);
    	this.speed = 0.2;
    	// Set stats
    	this.damage = 100;
    	this.shield = 240;
    	this.firepower = 3;
    }
    
    @Override
    public void update(World world, Camera cam, int delta) throws SlickException 
    {
    	super.update(world, cam, delta);
    	// Update only when on screen (y-wise)
    	if(world.reachedTop()) {
	    	double x = this.getX();
	    	double y = this.getY();
	        /* Calculate the amount to move in each direction, based on speed */
	        double amount = delta * this.getSpeed();
	        /* Check where The Boss is and flip the direction if necessary */
	        if(this.getX() <= xMin) {
	        	direction = -direction;
	        	x = xMin;
	        }
	        else  if(this.getX() >= xMax) {
	        	direction = -direction;
	        	x = xMax;
	        }
	        amount = amount * direction;
	        /* The new location, y doesn't change */
	        x += amount;
	        /* Fire missiles */
			if (this.getcooldown() == 0)
				world.missileList.add(this.fire());
	        /* Update missile cooldown */
	        update_cooldown(delta);
	        moveto(world, x, y);
    	}
    }
    
    @Override
    public Missile fire()
    	throws SlickException
    {
    	missile_cd = (300 - 80 * this.getFirePower());
    	return new Missile(this);
    }

    @Override
	public double getcooldown() {
    	return missile_cd;
	}
	
    @Override
	public void update_cooldown(int delta) {
        if (missile_cd > 0)
        	missile_cd -= delta;
        if (missile_cd < 0)
        	missile_cd = 0;
	}
}
