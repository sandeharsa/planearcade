/**
 * Item: an object that contains various powerups for the Player.
 * @author sandeharsa
 *
 */
abstract class Item extends GameObject 
{
	public Item(double x, double y)
	{
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Check collision between item and the Player.
	 * If the item collides with a player, apply item effect to Player.
	 * Also, remove the item from the item list in the game world
	 * @param world The game world.
	 */
	void update(World world)
	{
    	if (world.OnScreen(this))
    	{
    		if(world.getPlayer().intersects(this))
    		{
    			this.incollision(world.getPlayer());
    			world.getItemList().remove(this);
    		}
    	}
	}
	
	/**
	 * Applies the item effect to the player
	 * @param p The player
	 */
	abstract void effect(Player p);
	
	@Override
	void incollision(GameObject obj)
	{
		if (obj instanceof Player)
		{
			effect((Player)obj);
		}
	}
}
